import maker from './ribbonmaker'
import path from 'path'

function run() {
    // Check argument count
    if (process.argv.length < 3) {
        console.log("Error: Wrong arguments")
        console.log("")
        console.log("Usage: node index.js INPUT_FILE [OUTPUT_DIR]")
        process.exit(1)
    }

    // Load parameters
    const INPUT_FILE = process.argv[2]
    const OUTPUT_DIR = process.argv[3] || "out/"

    maker(INPUT_FILE, OUTPUT_DIR)
}

run()