import jimp from 'jimp'
import fs from 'fs'
import path from 'path'
import find from 'find'
import mkdirp from 'mkdirp'

export default function generateVariants(backgroundFile, outputDir) {
    const variantsDir = path.join(__dirname, '../', 'variants/')
    const presetsPath = path.join(__dirname, '../', 'presets.json')
    var presets = JSON.parse(fs.readFileSync(presetsPath, 'utf8'))
    find.file(/\.png$/, variantsDir, async (results) => {
        for (var i in results) {
            const filename = results[i]
            await generateVariant(backgroundFile, filename, outputDir, presets)
        }
    }, {
        dir: variantsDir,
        name: 'png'
    })
}

async function generateVariant(backgroundFile, overlayFile, outputDir, presets) {
    const variantName = path.basename(overlayFile, ".png")
    console.log("Variant: " + variantName)
    for (var i in presets) {
        const preset = presets[i]
        var filename = preset.filename.replace("%VARIANT%", variantName)
        var fullPath = outputDir + filename
        const dirname = path.dirname(fullPath)
        mkdirp.sync(dirname)
        await draw(backgroundFile, overlayFile, fullPath, preset.size)
    }
}

function draw(backgroundFile, overlayFile, outputFile, size) {
    return new Promise(
        (resolve, reject) => {
            jimp.read(backgroundFile).then((backgroundImage) => {
                    return jimp.read(overlayFile).then((overlayImage) => {
                        const result = backgroundImage.resize(size, size)
                            .composite(overlayImage.resize(size, size), 0, 0)
                            .write(outputFile)
                            resolve()
                        return result
                    }
                ).catch((err) => {
                    reject(err)
                })
            }).catch((err) => {
                reject(err)
            })
        }
    )
}