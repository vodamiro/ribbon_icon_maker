# Ribbon Icon Maker

A tool for creating icons for Android application and iOS application for different build environments.

![Input file](https://bytebucket.org/vodamiro/ribbon_icon_maker/raw/master/docs/icon.png) + ![Overlay file](https://bytebucket.org/vodamiro/ribbon_icon_maker/raw/master/docs/dev.png) => ![Output icon](https://bytebucket.org/vodamiro/ribbon_icon_maker/raw/master/docs/icon_dev.png)

![output](https://bytebucket.org/vodamiro/ribbon_icon_maker/raw/master/docs/out.png) 

## Usage

Run following command:

`npm start INPUT_FILE [OUTPUT_DIR]` where:

`INPUT_FILE` is a path to an icon PNG file with rectangular shape (recommended size: 1024px)

`[OUTPUT_DIR]` is optional path to a directory, where output images will be stored (default: `./out/`)

To customize which files will be created, edit file `presets.json`. Default structure looks like:

```json
[
    { "filename": "Android/%VARIANT%/res/mipmap/store.png", "size": 512 },
    { "filename": "Android/%VARIANT%/res/mipmap-ldpi/ic_launcher.png", "size": 36 },
    { "filename": "Android/%VARIANT%/res/mipmap-mdpi/ic_launcher.png", "size": 48 },
    { "filename": "Android/%VARIANT%/res/mipmap-hdpi/ic_launcher.png", "size": 72 },
    { "filename": "Android/%VARIANT%/res/mipmap-xhdpi/ic_launcher.png", "size": 96 },
    { "filename": "Android/%VARIANT%/res/mipmap-xxhdpi/ic_launcher.png", "size": 144 },
    { "filename": "Android/%VARIANT%/res/mipmap-xxxhdpi/ic_launcher.png", "size": 192 },

    { "filename": "iOS/%VARIANT%/store.png", "size": 1024 },
    { "filename": "iOS/%VARIANT%/app_icon@2x.png", "size": 120 },
    { "filename": "iOS/%VARIANT%/app_icon@3x.png", "size": 180 }
]
```

where each item defines path to output file with its own output image size. String `%VARIANT%` will be replaced with variant name, which is the name of variant overlay file without an extension.

To customize overlay variants (in this case ribbons), please see `variants` directory.


To run test, run following command: `npm test`. This command takes an icon stored in `test/icon.png` and creates output files according to `presets.json` and stores them in directory named `out`.

## TODO

[ ] Web application with fronted will be created (IN-PROGRESS)

## Authors

Miroslav Voda - vodamiro(at)gmail.com
Lubomir Baloun - lubosbaloun(at)gmail.com